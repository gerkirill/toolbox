Set of cli tools (requires php installed).

Installation:

wget https://bitbucket.org/gerkirill/toolbox/downloads/toolbox.phar

chmod +x ./toolbox.phar

sudo ./toolbox.phar install

After that you may run it like this:

  toolbox <subcommand>

Available subcommands:

 - install - create symlink in /usr/local/bin poining to toolbox.phar file 
 - update - update to the latest version
 - addhost - create apache virtual host
 - edithost - edit virtual host which corresponds to the current folder
 - errlog - view apache error log
 - ssh - connect over ssh to some insign project
 - time - interactive time calculator
 - mount - mount remote server forlder with sshfs
 - facl - apply symfony-like setfacl command to the listed folders
 - watchless - watch for the .less files changes and update .css accordingly
 
 Note: some subcommands require root permissions to work.