<?php
$subcommand = isset($argv[1]) ? $argv[1] : null;
$mainScript = $argv[0];
if (null === $subcommand) {
	$subcommands = array();
	$dir = new DirectoryIterator(__DIR__);
	foreach($dir as $entry) {
		if ($entry->isFile()) {
			$fileName = $entry->getFilename();
			if (false !== strpos($fileName, '_command')) {
				$commandName = str_replace('_command.php', '', $fileName);
				$subcommands[] = $commandName;
			}
		}
	}
	sort($subcommands);
	echo "Available subcommands:\n";
	foreach($subcommands as $subcommand) {
		echo '  '.$subcommand."\n";
	}
	die();
}

$subcommandFile = sprintf('%s/%s_command.php', __DIR__, $subcommand);
if (file_exists($subcommandFile)) {
	array_shift($argv);
	include($subcommandFile);
} else {
	echo "Unknown subcommand '$subcommand'.\n";
}