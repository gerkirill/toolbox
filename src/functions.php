<?php
function writeln($s) {
	$args = func_get_args();
	if (1 == count($args)) {
		echo $s;
	} else {
		call_user_func_array('printf', $args);
	}
	echo "\n";
}

function requireRootUser() {
	if ('root' != trim(`whoami`)) {
		writeln('Root user is required to run this command.');
		// todo: ask for su password
		die;	
	}
}

function prompt($prompt, $pass=false) {
	echo "$prompt: ";
	if ($pass) {
		$val = preg_replace('/\r?\n$/', '', `stty -echo; head -n1; stty echo`);
	} else {
		$val = preg_replace('/\r?\n$/', '', `head -n1`);
	}	
	return $val;
}

function savePreset($subcommand, $preset, $data) {
	$userHome = $_SERVER['HOME'];
	$settingsDir = $userHome . '/.toolbox';
	if (!is_dir($settingsDir)) mkdir($settingsDir);
	$settingsFile = sprintf('%s/%s_%s.ini', $settingsDir, $subcommand, $preset);
	$presetContentLines = array();
	foreach($data as $k=>$v) {
		$presetContentLines[] = sprintf('%s = "%s"', $k, $v);
	}
	$presetContent = implode("\n", $presetContentLines);
	file_put_contents($settingsFile, $presetContent);
	chmod($settingsFile, 0600);
}

function loadPreset($subcommand, $preset) {
	$userHome = $_SERVER['HOME'];
	$settingsDir = $userHome . '/.toolbox';
	if (!is_dir($settingsDir)) return array();
	$settingsFile = sprintf('%s/%s_%s.ini', $settingsDir, $subcommand, $preset);
	if (!file_exists($settingsFile)) return array();
	$data = parse_ini_file($settingsFile);
	if (false === $data) return array();
	return $data;
}

function showPreset($data) {
	foreach($data as $k=>$v) {
		$valueDecorated = (false === stripos($k, 'pass')) ? $v : str_repeat('*', strlen($v));
		writeln('%s = %s', $k, $valueDecorated);
	}
}