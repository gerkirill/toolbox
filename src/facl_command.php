<?php
/*
usage : sudo toolbox setfacl -u `whoami` <path1> <path2>
sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs
sudo setfacl -dR -m u:www-data:rwx -m u:`whoami`:rwx app/cache app/logs

note: require root, take default user from $_SERVER['SUDO_USER'] - if available
*/
require_once(__DIR__.'/functions.php');
if (count($argv)<2) {
	writeln('Usage: sudo toolbox facl <directory> [<directory2> ...]');
	die;
}
requireRootUser();
// use username from $_SERVER['SUDO_USER'] or ask for it
$currentUser = isset($_SERVER['SUDO_USER']) ? $_SERVER['SUDO_USER'] : null;
if (!$currentUser) {
	$currentUser = prompt('What is your username?');
}
$wwwUser = 'www-data';
// use www-data user if it exists or ask for it otherwise
$groupSearchResult = `cat /etc/group | grep -e "^$wwwUser:"`;
$groupSearchResult = trim($groupSearchResult);
if (empty($groupSearchResult)) {
	$wwwUser = prompt('What is apache user name?');
}
writeln('Assume your username is "%s" and apache one is "%s".', $currentUser, $wwwUser);
writeln('Will now execute setfacl.');
$arguments = $argv;
// remove subcommand name from arguments
array_shift($arguments);
$cwd = getcwd();
foreach($arguments as $path) {
	if ('/' !== substr($path, 0, 1)) {
		$path = $cwd . '/' .$path;
	}
	if (!is_dir($path)) {
		writeln('%s is not a directory, skipped.', $path);
		continue;
	}
	writeln('Will apply setfacl to %s.', $path);
	$pathEscaped = escapeshellarg($path);
	$command_1 = sprintf('setfacl -R -m u:%s:rwX -m u:%s:rwX %s', $wwwUser, $currentUser, $pathEscaped);
	$command_2 = sprintf('setfacl -dR -m u:%s:rwx -m u:%s:rwx %s', $wwwUser, $currentUser, $pathEscaped);
	echo `$command_1`;
	echo `$command_2`;
}
writeln('Done.');