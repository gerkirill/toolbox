<?php
// edit virtual host corresponding to the current folder
require_once(__DIR__.'/functions.php');
requireRootUser();
$cwd = getcwd();
//  /some/www/path => array('', 'some', 'www', 'path')
$cwdParts = explode('/', $cwd);
$searchResults = array();
while(count($cwdParts) > 1) {
	$path = implode('/', $cwdParts);
	$command = sprintf('grep -lr %s %s', escapeshellarg($path), escapeshellarg('/etc/apache2/sites-available'));
	$searchResult = trim(`$command`);
	$searchResults = array_filter(array_map('trim', explode("\n", $searchResult)));
	if (count($searchResults)) {
		break;
	}
	array_pop($cwdParts);
}
$matchCount = count($searchResults);
if (0 === $matchCount) {
	writeln('Sorry, can not find matching virtual host');
} elseif (1 === $matchCount) {
	writeln('Virtual host %s matches folder %s.', $searchResults[0], $path);
	$editCommand = sprintf('gedit %s', escapeshellarg($searchResults[0]));
	`$editCommand`;
	// remove tmp file after gedit
	$tmpFile = $searchResults[0].'~';
	if (file_exists($tmpFile)) unlink($tmpFile);
} else {
	if ($path !== $cwd) {
		writeln('No virtual hosts match current foler (%s).', $cwd);
	}
	writeln('Multiple hosts match folder %s:', $path);
	foreach($searchResults as $searchResult) {
		writeln($searchResult);
	}
}