<?php
// interactive time calculator:
// 8.4 - :25 - 1:10 => xx:xx or xx.yy
require_once(__DIR__.'/functions.php');
while(true) {
	$expr = prompt('$> ');
	// replace :dd with .ss, evaluate, show result
	$replacements = array();
	if (preg_match_all('/:\d+/', $expr, $matches)) {
		foreach($matches[0] as $match) {
			$dec = (ltrim($match, ':') / 60);
			$decParts = explode('.', $dec);
			$replacements[$match] = '.'.$decParts[1];
		}
		$expr = strtr($expr, $replacements);	
	}
	// we have result of previous calculation
	if (!empty($res)) {
		// if expression starts with operator
		$firstChar = substr(ltrim($expr), 0, 1);
		if (in_array($firstChar, array('+', '-', '*', '/'))) {
			$expr = $res . ' ' . $expr;
		}
	}
	$expr = '$res = '.$expr.';';
	$res = null;
	@eval($expr);
	// todo: also output result in laternative format
	if (null === $res) {
		writeln('You have an error in your expression.');
	} else {
		writeln($res);
	}
}