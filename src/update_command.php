<?php
// update toolbox.phar whenever it is localed (look by symlink in /usr/local/bin)
require_once(__DIR__.'/functions.php');
$symlink = '/usr/local/bin/toolbox';

// script was started with toolbox.phar file
if (preg_match('/\.phar$/', $mainScript)) {
	if ('/' === substr($mainScript, 0, 1)) {
		$pharPath = $mainScript;
	} else {
		$pharPath = realpath(getcwd().'/'.$mainScript);
	}
	$symlinkTarget = @readlink($symlink);

	// the .phar file running isn't the one global symlink points to - 
	// so ask which one to update
	if ($symlinkTarget && $symlinkTarget != $pharPath) {
		writeln('You are running %s file,', $pharPath);
		writeln('But global "toolbox" symlink points to %s.', $symlinkTarget);
		$answer = strtolower(prompt('Do you want to update global toolbax.par file? (y/N) [N]'));
		if ('y' === $answer) {
			writeln('Updating global toolbax.phar (%s).', $symlinkTarget);
			$pharToUpdte = $symlinkTarget;
		} else {
			writeln('Updating toolbax.phar you run (%s).', $pharPath);
			$pharToUpdte = $pharPath;
		}
	} else {
		$pharToUpdte = $pharPath;
	}
// script is running with "toolbox" symlink - update its destination .phar file
} else {
	$pharToUpdte = readlink($symlink);
}
// get fresh toolbox.phar, write it in place and make executable
writeln('Downloading latest version...');
$newPharContent = file_get_contents('https://bitbucket.org/gerkirill/toolbox/downloads/toolbox.phar');
writeln('Replacing current toolbox.phar with the new version.');
file_put_contents($pharToUpdte, $newPharContent);
chmod($pharToUpdte, 0755);
writeln('Done.');