<?php
require_once(__DIR__.'/functions.php');

/*
TODO: add notifications with notify-send updated 'style.css\
test.css' for ubuntu and probably knotify for KDE
Also - handle non-zero exit status of lessc and notify on that as well

- allow to start with params and display usage
*/

$output = trim(`which lessc`);
if (empty($output)) {
	writeln('You should have lessc command installed to use this tool.');
	die;
}

$output = trim(`which inotifywait`);
if (empty($output)) {
	writeln('You should have inotifywait command installed to use this tool.');
	writeln('You can install it by running:');
	writeln('  sudo apt-get install inotify-tools');
	die;
}

$preset = loadPreset('watchless', md5(getcwd()));
$reuse = false;
if (!empty($preset)) {
	writeln('Last time you used this command in this directory with the next parameters:');
	showPreset($preset);
	$answer = strtolower(prompt('Use same settings again?(Y/n) [Y]'));
	$reuse = ('n' !== $answer);
}

if ($reuse) {
	$lessPath = $preset['less'];
	$cssPath = $preset['css'];
	$lessDir = $preset['watch'];
}
else {
	$lessPath = '';
	while(!$lessPath) {
		$answer = trim(prompt('What\'s the input .less file?'));
		if ($answer) {
			$path = getAbsPath($answer);
			if (file_exists($path)) {
				$lessPath = $path;
				break;
			}
		}
		writeln('Sorry, can not find such file, try again.');
	}

	$cssPath = '';
	while(!$cssPath) {
		$answer = prompt('What\'s the ouput .css file?');
		$path = getAbsPath($answer);
		if (!file_exists($path)) {
			$createRes = @file_put_contents($path, '');
			if (false === $createRes) {
				writeln('Sorry, can not create output file %s', $path);
				continue;
			}
		}
		$cssPath = $path;
	}

	$answer = prompt('What\'s the directory to watch for changes?');
	$lessDir = getAbsPath($answer);
}


$lessPathEsc = escapeshellarg($lessPath);
$cssPathEsc = escapeshellarg($cssPath);
$lessDirEsc = escapeshellarg($lessDir);

savePreset('watchless', md5(getcwd()), array(
	'less' => $lessPath,
	'css' => $cssPath,
	'watch' => $lessDir
));

writeln('Ok, starting to watch. Press Ctrl+C to terminate');

$notify = ('' !== trim(`which notify-send`));

while(true) {
	$cmdOut = trim(`inotifywait -rq -e modify -e delete -e create --format "%w%f;%e" $lessDirEsc $lessPathEsc`);
	writeln($cmdOut);
	$exitCode = 0;
	passthru("lessc $lessPathEsc > $cssPathEsc", $exitCode);
	if ($notify) {
		if (0 === $exitCode) {
			`notify-send -i "/usr/share/pixmaps/toolbox_ok.png" ".less watch" "css file updated"`;
		} else {
			`notify-send -i "/usr/share/pixmaps/toolbox_error.png" ".less watch" "error updating css file"`;
		}
	}
}
//inotifywait -rq -e modify -e delete -e create --format "%w%f;%e" .
//./asdf;CREATE

function getAbsPath($path) {
	if ('/'===substr($path, 0, 1)) return $path;
	return getcwd().'/'.$path;
}